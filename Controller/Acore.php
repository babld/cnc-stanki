<?php

	abstract class Acore {
		protected $db;
		
		public function __construct() {
			$this->db = Db::getInstance();
		}
		
		abstract function getBody();
		
	}
	
?>
