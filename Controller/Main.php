<?php

	class Main extends Acore {
		
		public function getContent() {
			echo "get content";
		}
		
		public function getBody() {
			
			if(isset($_GET['ajax']) and !empty($_GET['ajax'])) {
				
				$from = "no-reply@cnc-stanki.ru";
				
				$headers	= 'From:' . $from . "\r\n" . 'Content-type: text/html; charset=UTF-8'  . "\r\n";
				
				$name	 = (isset($_POST["name"]) and !empty($_POST["name"]))			? $_POST["name"]		: "Неизвестно";
				$phone	 = (isset($_POST["phone"]) and !empty($_POST["phone"]))			? $_POST["phone"]		: null;
				$email	 = (isset($_POST["email"]) and !empty($_POST["email"]))			? $_POST["email"]		: null;
				$mess	 = (isset($_POST["message"]) and !empty($_POST["message"]))		? $_POST["message"]		: null;
				$mess	 = (isset($_POST["model"]) and !empty($_POST["model"]))			? $_POST["model"]		: null;
				$options = (isset($_POST["options"]) and !empty($_POST["options"]))		? $_POST["options"]		: null;
				$user_request = "";
				
				if($email or $mess) {
					$subject = "ЧПУ: Запрос с формы";
					$mailHeader = "Здравствуйте, на Вашем сайте ЧПУ оставлена заявка с формы обратной связи<br/><br/><br/>";
				} else {
					$subject = "ЧПУ: Заявка на обратный звонок";
					$mailHeader = "Здравствуйте, на Вашем сайте ЧПУ оставлена заявка на обратный звонок<br/><br/><br/>";
				}
				
				$message = $mailHeader;
				
				if($name) $message .= "Имя: $name <br/>";
				if($phone) $message .= "Телефон: $phone <br/>";
				if($email) $message .= "Почта: $email <br/>";
				if($mess) $message .= "Сообщение: $mess <br/>";
				if($model) $message .= "Moдель: $model <br/>";
				
				$options = "";
				foreach($_POST["options"] as $key => $value) {
					if($key == 0) $options .= "Дополнительные опции <br/>";
					$options .= $key + 1 . ") $value;<br/>";
				}
				
				$message .= "<br/>$options";
				
				$message .= "<br/>IP адрес: " . $_SERVER["REMOTE_ADDR"];
				
				if(@mail(ADMIN_EMAIL, $subject, $message , $headers)) {
					if($email) {
						if($name) $user_name = "<span style='text-transform:uppercase'>$name</span>,";
						if($mess) $user_request = "<br/>Вы писали: \"$mess\".<br/>";
						
						$user_mess = "Здравствуйте, $user_name. Ваша заявка принята!<br/>
						
						$user_request
						<br/>
						В ближайшее время мы свяжемся с Вами.<br/>
						Фрезерные станки марки “SuperStar” – оборудование от ведущего производителя.<br/>
						<br/>
						С уважением, Дмитрий<br/>
						моб.: +7 983 314 54 88<br/>
						skype: coswart<br/>";
						
						@mail($email, "Заявка на сайте фрезерно-гравировальных станков cnc-stanki.ru", $user_mess, $headers);
					}
					echo "{send:ok}";
				} else {
					echo "{send:false}";
				}
				
			} else {
				
				#Currency 
				$url	= "http://www.cbr.ru/scripts/XML_daily.asp?date_req=" . date("d.m.Y");
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 4);
				curl_setopt($ch, CURLOPT_TIMEOUT, 7);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
				
				$file	= ROOT . "/tmp/currency.txt";
				$fp 	= fopen($file, "r");
				
				if(($content = curl_exec($ch)) !== false) {
					echo "<!--";
					$XML	= simplexml_load_file($url);
					echo "-->";
					if($XML) {
						$usd 	= $XML->xpath('/ValCurs/Valute[@ID="R01235"]/Value');
						$usd 	= round((float)str_replace(',', '.', $usd[0]), 2);
						
						$fp 	= @fopen($file, "w");
						@fwrite($fp, $usd);
						
					} else {
						$usd = @fread($fp, 20);
						$usd = (float)$usd;
					}
				} else {
					$usd = @fread($fp, 20);
					$usd = (float)$usd;
				}
				
				@fclose($fp);
				
				include "View/index.php";
			}

		}
		
	}

?>