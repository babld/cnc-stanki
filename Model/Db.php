<?php

	class Db {
		
		private static $instance = null;
		
		public function getInstance() {
			
			if(self::$instance === null) {
				#self::$instance = new PDO('mysql:host=' . DBHOST . ';dbname=' . DBNAME, DBUSER, DBPASS);
			}
			return self::$instance;
		}
		
		private function __construct() {}
		private function __clone() {}
		private function __wakeup() {}
		
	}

?>