$(document).ready(function(){
	jQuery.validator.addMethod("allPhoneFormat", function (value, element) {
		return this.optional(element) || /^[0-9()\-+ ]{7,}$/.test(value);
	}, "Введите корректно.");

	$( ".city-switcher" ).click(function(){
		var city = $(this).attr("data-city");
		ymapsLoader(city);
	});
	
	$("form").each(function (i) {
		$(this).staFeedback();
	});
	
	// модальные окна
	$(".open-modal-order").click(function() {

        var modal = $(this).attr("modal");
        $(".modal-order").find("[name=modal]").val(modal);

		//event.preventDefault();
		$('#overlay').off("click");
		$('#overlay').fadeIn(400, function() {
			$(".modal-order")
				.css('display', 'block')
				.animate({opacity: 1, top: '50%'}, 200, function() {
					$('#overlay').on("click", function(){
						$(".modal-order").animate({opacity: 0, top: '45%'}, 200, function() {
							$(this).css('display', 'none');
							$('#overlay').fadeOut(400);
						});
					});
				});
		});	
	});
	$(".open-modal-quest").click(function() {
		//event.preventDefault();
		$('#overlay').off("click");
		$('#overlay').fadeIn(400, function() {
			$(".modal-quest")
				.css('display', 'block')
				.animate({opacity: 1, top: '50%'}, 200, function() {
					$('#overlay').on("click", function(){
						$(".modal-quest").animate({opacity: 0, top: '45%'}, 200, function() {
							$(this).css('display', 'none');
							$('#overlay').fadeOut(400);
						});
					});
				});
		});
		return false;
	});
	
    function cityIP(){
        $.getJSON('/user_info.js.php',
		
        function(result) {
			var ip = result.ip;
			  
			$.ajax({
				type: "GET",
				url: "http://ipgeobase.ru:7020/geo?ip="+ip,
				dataType: "xml",
				cache: false,
				success: function(xml) {
					var region = $(xml).find('region').text();
					var district = $(xml).find('district').text();
                    var addressContainer = $(".city-phone-text");
                    var phoneContainer = $(".header-contacts .city-phone");
					var cityAbbr = "nsk";
				   
					switch(district){					
						case "Центральный федеральный округ":
							phoneContainer.html("<a href=\"tel:+74993466799\">8-499-346-67-99</a>");
							addressContainer.html("<a href=\"tel:+74993466799\">Москва</a>");
							cityAbbr = "msk";
						break;
						
						case "Северо-Западный федеральный округ":
							phoneContainer.html("<a href=\"tel:+78124243313\">8-812-424-33-13</a>");
							addressContainer.html("<a href=\"tel:+78124243313\">Санкт-Петербург</a>");
							cityAbbr = "spb";
						break;	
						
						case "Уральский федеральный округ":
							phoneContainer.html("<a href=\"tel:+73433456532\">8-343-345-65-32</a>");
							addressContainer.html("<a href=\"tel:+73433456532\">Екатеринбург</a>");
							cityAbbr = "ekb";
						break;
						default:
							phoneContainer.html("<a href=\"tel:+73832078860\">8-383-207-88-60</a>");
                            addressContainer.html("<a href=\"tel:+73832078860\">Новосибирск</a>");
							cityAbbr = "nsk";
					}

					switch(region){
						case "Омская область":
							phoneContainer.html("<a href=\"tel:+73812972030\">8-381-297-20-30</a>");
							addressContainer.html("Омск");
							cityAbbr = "omsk";
						break;
						
						/*case "Томская область":
							phoneContainer.html("8 (381) 297-20-30");
						break;	*/													
					}  
					ymapsLoader(cityAbbr)
				},
				error: function() { cityIP();  }
			});
		});
        
    }
    
    cityIP();
	
	$("a[rel=lightbox]").fancybox();
	
	$('.fancybox').fancybox({
		padding: 15,
		scrolling: 'auto'
	});
	
});