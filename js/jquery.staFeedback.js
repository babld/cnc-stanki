(function( $ ){
    $.fn.staFeedback = function(options) {
        
        var settings = $.extend({}, options);

        var thisForm = this;

        this.validate(
            {
                rules: {
                    name: {
                        required:true
                    },
                    phone:{
                        required: true,
                        allPhoneFormat: true
                    },
                    email:{
                        required: true,
                        email:true
                    }
                },

                messages:{
                    email:{
                        required: "Введите E-mail",
                        email: "Неверный формат"
                    },
                    phone:{
                        required: "Введите телефон",
                        allPhoneFormat: "Введите корректно"
                    },
                    name: {
                        required:"Введите имя"
                    }
                },

                ignore: ".ignore",

                submitHandler: submit
            }
        );
        //this.find("input[name='phone']").mask("?(999) 999-99-99");

        function submit(form){
            $.post(
                 '/?ajax=1',
                $(form).serialize(),
                parseResponce);
        }

        function parseResponce(response) {
            thisForm.trigger('reset');
            thisForm.replaceWith('<div class="order-success">Ваша заявка<br>успешно отправлена</div>')
            //$(".modal-order").css("opacity", 0).hide();
            //$(".modal-thx").css({opacity:1, top:"50%"}).show();
        }
    };
})(jQuery);