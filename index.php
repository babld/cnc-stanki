<?php
	session_start();
	$dir_name = str_replace('\\', '/', dirname(__FILE__));
	define('ROOT', $dir_name);
	
	require_once(ROOT . "/config.php");
	require_once(ROOT . "/Controller/CoreAdmin.php");
	
	function __autoload($c) {
		$controllerClasses = "Controller/$c.php";
		$modelClasses = "Model/$c.php";
		
		if(file_exists($controllerClasses)) {
		
			require_once($controllerClasses);
		
		} elseif($modelClasses) {
		
			require_once($modelClasses);
		
		}
	}
	
	$obj = new Main();
	$obj->getBody();
	
?>