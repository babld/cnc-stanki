<div class="footer" id="footer">
	<div class="footet-title">Остались вопросы?</div>
	<div class="footer_feedback">
		<div class="footer_feedback-form">
			<form method="POST">
				<div class="input-field">
					<input type="text" name="name" placeholder="Имя" class="footer_feedback-name"/>
				</div>
				<div class="input-field email">
					<input type="text" name="email" placeholder="Email" class="footer_feedback-email"/>
				</div>
				<div class="input-field phone">
					<input type="text" name="phone" placeholder="Телефон" class="footer_feedback-phone"/>
				</div>
				<textarea name="message" class="footer_feedback-mess" placeholder="Задайте Ваши вопросы по поставке фрезерных станков с ЧПУ специалистам нашей компании"></textarea>

				<div class="footer_feedback-notice">Мы оперативно ответим на все вопросы!</div>
				<input type="submit" value="Получить консультацию" class="footer_feedback-submit"/>
			</form>
		</div>
		<div class="footer_feedback-manager">
			<img src="/images/manager.jpg"/>
		</div>
		<div class="footer_feedback-text">
			<div class="manager-name">Дмитрий</div>
			<div class="manager-range">Консультант по продаже фрезерных станков с ЧПУ</div>
			<div class="footer_feedback-bottom-text">Оставьте заявку<br/>и наш специалист<br/> оперативно проконсультирует Вас!</div>
		</div>
	</div>

	<div class="tabs">
		<div class="footer-cityes" id="contacts">
			<table class="cityes-list">
				<tr>
					<td>
						<div class="city-name"><a class="city-switcher" data-city="nsk">Новосибирск</a></div>
						<div class="city-phone"><a class="city-switcher" data-city="nsk">8 (383) 207-88-60</a></div>
					</td>
					<td>
						<div class="city-name"><a class="city-switcher" data-city="msk">Москва</a></div>
						<div class="city-phone"><a class="city-switcher" data-city="msk">8 (499) 346-67-99</a></div>
					</td>
					<td>
						<div class="city-name"><a class="city-switcher" data-city="spb">Санкт-Петербург</a></div>
						<div class="city-phone"><a class="city-switcher" data-city="spb">8 (812) 424-33-13</a></div>
					</td>
					<td>
						<div class="city-name"><a class="city-switcher" data-city="ekb">Екатеринбург</a></div>
						<div class="city-phone"><a class="city-switcher" data-city="ekb">8 (343) 345-65-32</a></div>
					</td>
					<td>
						<div class="city-name"><a class="city-switcher" data-city="omsk">Омск</a></div>
						<div class="city-phone"><a class="city-switcher" data-city="omsk">8 (381) 297-20-30</a></div>
					</td>
				</tr>
			</table>
		</div>
		
		<div id="YMapsID"></div>
	</div>
	<div class="footer-bottom">
		<div class="header-box w-clear">
			<div class="header-logo">
				<a href="http://sibtransasia.ru" target="_blank">	
					<img src="/images/logo.png"/>
				</a>
			</div>
			<div class="main-title">Фрезерные станки с ЧПУ</div>
			<div class="header-contacts">
				<div class="w-clear">
					<div class="free-phone">
						<a href="tel:8-800-775-67-58">8-800-775-67-58</a>
					</div>
					<div class="free-phone-text">
						<a href="tel:8-800-775-67-58">звонок бесплатный</a>
					</div>
				</div>
				<div class="w-clear">
					<div class="city-phone">
						<a href="tel:8-383-207-88-60">8-383-207-88-60</a>
					</div>
					<div class="city-phone-text">
						<a href="tel:8-383-207-88-60">Новосибирск</a>
					</div>
				</div>
				<div class="w-clear">
					<div class="header-email-text">Эл. почта:</div>
					<div class="header-email">
						<a href="mailto:<?=$kodermail?>"><?=$kodermail?></a>
					</div>
				</div>
				<div class="">
					<div class="header-feedback">
						<form>
							<input name="phone" class="feedback-phone" type="text" placeholder="Ваш телефон"/>
							<input class="feedback-submit" type="submit" value="Перезвоните мне"/>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter29418245 = new Ya.Metrika({id:29418245,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/29418245" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<?php
	if($_SERVER["HTTP_HOST"] != "cnc-stanki")
		echo '<script type="text/javascript" src="//perezvoni.com/files/widgets/1636-fd005d5360accbb15-4864e5267fd005d-267fd005d5360acc.js" charset="UTF-8"></script>';?>
	
</body></html>
