<div class="matching-block" id="sravnenie">
	<div class="matching-container">
		<div class="matching-container-title">Сравнение характеристик фрезерных станков с ЧПУ</div>
		<div class="match-table">
			<div class="row first-row w-clear">
				<div class="first first-image">Фото<br/>фрезерного<br/>станка</div>
				<div class="row-item">
					<div class="match-image">
						<a href="/images/models/superstar-cx1325-full.jpg" class="fancybox"><img src="/images/models/superstar-cx1325-172x151.png"/></a>
					</div>
				</div>
				<div class="row-item">
					<div class="match-image">
						<a href="/images/models/superstar-cxm1325-full.jpg" class="fancybox"><img src="/images/models/superstar-cxm1325-172x151.png"/></a>
					</div>
				</div>
				<div class="row-item">
					<div class="match-image">
						<a href="/images/models/superstar-m25-full.jpg" class="fancybox"><img src="/images/models/superstar-m25-161x151.png"/></a>
					</div>
				</div>
				<div class="row-item no-mr">
					<div class="match-image">
						<a href="/images/models/superstar-cxg6090-full.jpg" class="fancybox"><img src="/images/models/superstar-grg6090-134x150.png"/></a>
					</div>
				</div>
			</div>
			<div class="row w-clear">
				<div class="first">Модель</div>
				<div class="row-item">CX1325</div>
				<div class="row-item">CXM1325</div>
				<div class="row-item">M25</div>
				<div class="row-item no-mr">CXG6090</div>
			</div>
			<div class="row w-clear">
				<div class="first">Шпиндель</div>
				<div class="row-item">3 кВт</div>
				<div class="row-item">3 кВт</div>
				<div class="row-item">4.5 кВт</div>
				<div class="row-item no-mr">1.5 кВт</div>
			</div>
			<div class="row w-clear">
				<div class="first">Рабочий стол</div>
				<div class="row-item">Без вакуумного стола</div>
				<div class="row-item">Без вакуумного стола</div>
				<div class="row-item">Вакуумный стол</div>
				<div class="row-item no-mr">Без вакуумного стола</div>
			</div>
			<div class="row w-clear two-line">
				<div class="first">Конструкция</div>
				<div class="row-item">Стальные трубы квадратного сечения</div>
				<div class="row-item">Стальные трубы квадратного сечения</div>
				<div class="row-item">Стальные трубы квадратного сечения</div>
				<div class="row-item no-mr">Стальные трубы квадратного сечения</div>
			</div>
			<div class="row w-clear two-line">
				<div class="first">Опоры</div>
				<div class="row-item">6 тонких опор</div>
				<div class="row-item">4 толстых опоры либо T-фигура</div>
				<div class="row-item">6 толстых опор</div>
				<div class="row-item no-mr">Стальные опоры</div>
			</div>
			<div class="row w-clear">
				<div class="first">Системы управления</div>
				<div class="row-item">NCStudio</div>
				<div class="row-item">NCStudio</div>
				<div class="row-item">DSP</div>
				<div class="row-item no-mr">NCStudio</div>
			</div>
			<div class="row w-clear two-line">
				<div class="first">Частота вращения шпинделя об/мин</div>
				<div class="row-item">0-24000</div>
				<div class="row-item">0-24000</div>
				<div class="row-item">0-24000</div>
				<div class="row-item no-mr">0-24000</div>
			</div>
			<div class="row w-clear two-line">
				<div class="first">Точность позиционирования</div>
				<div class="row-item">+-0.05 мм</div>
				<div class="row-item">+-0.05 мм</div>
				<div class="row-item">+-0.05 мм</div>
				<div class="row-item no-mr">+-0.05 мм</div>
			</div>
			<div class="row w-clear">
				<div class="first">Охлаждение шпинделя</div>
				<div class="row-item">Воздушное</div>
				<div class="row-item">Воздушное</div>
				<div class="row-item">Водяное</div>
				<div class="row-item no-mr">Водяное</div>
			</div>
			<div class="row w-clear">
				<div class="first">Электрошкаф</div>
				<div class="row-item">самостоятельный</div>
				<div class="row-item">самостоятельный</div>
				<div class="row-item">самостоятельный</div>
				<div class="row-item no-mr">Вместе со станком</div>
			</div>
			<div class="row w-clear two-line end-row">
				<div class="first">Потребление энергии (без шпинделя)</div>
				<div class="row-item">1.85 кВт</div>
				<div class="row-item">1.85 кВт</div>
				<div class="row-item">1.85 кВт</div>
				<div class="row-item no-mr">1.85 кВт</div>
			</div>
			
		</div>
	</div>
</div>