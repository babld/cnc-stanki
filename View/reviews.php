<div class="reviews">
	<div class="review-inner">
		<div class="review-inner-title">Отзывы клиентов</div>
		<div class="review-item left-col review1">
			<div class="reiview-image">
				<img src="/images/reviews/client1.png" class="client-photo"/>
				<img src="/images/reviews/client-frame-1.png" class="photo-frame"/>
			</div>
			<div class="review-name">
				Борис Петров<br/>
				г. Москва
			</div>
			<div class="review-text">
				Занимаемся изготовлением фасадов МДФ для корпусной мебели, заказали станок CX-1325. Привезли станок в Москву из Китая за 25 дней,
				подключили на следующий день после доставки. Порадовало качество исполнения конструктивных элементов, продуманность конструкций.
			</div>
		</div>
		<div class="review-item right-col review2">
			<div class="reiview-image">
				<img src="/images/reviews/client2.png" class="client-photo"/>
				<img src="/images/reviews/client-frame-2.png" class="photo-frame"/>
			</div>
			<div class="review-name">
				Дмитрий Каменев<br/>
				г. Красноярск
			</div>
			<div class="review-text">
				Задался вопросом выбора фрезерного станка с ЧПУ для обработки искусственного камня. Дмитрий помог подобрать мне нужную модель и оформить с ним заказ
				на фабрике. Нужный станок у фабрики оказался в наличии, оперативно оплатили его и привезли в Новосибирск за 2 недели.
			</div>
		</div>
		<div class="review-item left-col review3">
			<div class="reiview-image">
				<img src="/images/reviews/client3.png" class="client-photo"/>
				<img src="/images/reviews/client-frame-3.png" class="photo-frame"/>
			</div>
			<div class="review-name">
				Иван Диденко<br/>
				г. Томск
			</div>
			<div class="review-text">
				Необходимо было приобрести 2 станка М25 фабрики Superstar. Обрабатываем дерево на станках этой модели 4 года, все полностью устраивает.
				В наличии в России этих станков не оказалось. Нашли Ваш сайт в интернете, заказали товар и через 22 дня уже установили их на производство
				в Томске.
			</div>
		</div>
		<div class="review-item right-col review4">
			<div class="reiview-image">
				<img src="/images/reviews/client4.png" class="client-photo"/>
				<img src="/images/reviews/client-frame-4.png" class="photo-frame"/>
			</div>
			<div class="review-name">
				Марк Уткин<br/>
				г. Казань
			</div>
			<div class="review-text">
				Для расширения производства заказали 3 станка для обработки дерева у компании СибирьТрансАзия. Очень понравилась цена. В России в наличии за эти деньги
				станков с ЧПУ приемлемого качества найти не удалось. Цену зафиксировали при заказе, доставили станки в срок.
			</div>
		</div>
	</div>
</div>