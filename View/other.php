<div class="other-models">
	<div class="other-models-container">
		<div class="other-main-title block-title">
			Также мы поставляем следующее оборудование
		</div>
		
		<div class="other-models-wrap w-clear">
			<div class="other-model">
				<div class="other-image">
					<img src="/images/models/other1.png"/>
				</div>
				<div class="other-title">
					Фрезерные станки по металлу с ЧПУ
				</div>
				<a href="#footer" data-info="Интересует информация по фрезерным станкам по металлу с ЧПУ?" class="other-order">Получить информацию</a>
			</div>
			<div class="other-model">
				<div class="other-image">
					<img src="/images/models/other2.png"/>
				</div>
				<div class="other-title">
					Фрезерно-гравировальные станки по камню с ЧПУ
				</div>
				<a href="#footer" data-info="Интересует информация по фрезерно-гравировальным станкам по камню с ЧПУ?" class="other-order">Получить информацию</a>
			</div>
			<div class="other-model">
				<div class="other-image">
					<img src="/images/models/other3.png"/>
				</div>
				<div class="other-title">
					Многошпиндельные станки с ЧПУ
				</div>
				<a href="#footer" data-info="Интересует информация по многошпиндельным станкам ЧПУ?" class="other-order">Получить информацию</a>
			</div>
			<div class="other-model no-mr">
				<div class="other-image">
					<img src="/images/models/other4.png"/>
				</div>
				<div class="other-title">
					Станки для плазменной резки с ЧПУ
				</div>
				<a href="#footer" data-info="Интересует информация по фрезерным станкам по металлу с ЧПУ?" class="other-order">Получить информацию</a>
			</div>
		</div>
		<div class="other-model-choose">
			Кроме того, мы можем скомплектовать для Вас практически любой станок с ЧПУ под Ваши требования!
		</div>
		<a href="#footer" data-info="Закажите свой станок с ЧПУ" class="other-choose-order">Заказать свой станок</a>
	</div>
</div>