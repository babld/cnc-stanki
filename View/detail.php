<div class="detail-block" id="characteristics">
	<div class="detail-block-container w-clear">
		
		<div class="detail-block-title">Фрезерно-гравировальный станок с ЧПУ</div>
		<div class="detail-select select-wrap">
			<div class="controls">
				<div class="arrow"></div>
				<div class="label">Superstar CX1325</div>
			</div>
			<select>
				<option value="cx1325">Superstar CX1325</option>
				<option value="cxm1325">Superstar CXM1325</option>
				<option value="m25">Superstar M25</option>
				<option value="cxg6090">Superstar CXG6090</option>
			</select>
		</div>
		<div class="clear"></div>
		<div class="detail-images">
			<div class="detail-title">Фото станка</div>
			<div class="detail-gallery">
				<div class="detail-main-image w-clear">
					<a class="fancybox" href="/images/models/superstar-cx1325-full.jpg">
						<img data-model="cx1325" src="/images/gallery/cx1325.png"/>
					</a>
				</div>
				<div class="detail-thumbs w-clear">
					<div class="detail-thumb first">
						<a href="/images/gallery/cx1325.png">
							<img data-index="1" src="/images/gallery/thumb-cx1325.png"/>
						</a>
					</div>
					<div class="detail-thumb second">
						<a href="/images/gallery/cx1325-2.jpg">
							<img data-index="2" src="/images/gallery/thumb-cx1325-2.jpg"/>
						</a>
					</div>
					<div class="detail-thumb third no-bg">
						<a href="/images/gallery/cx1325-3.jpg">
							<img data-index="3" src="/images/gallery/thumb-cx1325-3.jpg"/>
						</a>
					</div>
				</div>
				
			</div>
		</div>
		<div class="detail-tech">
			<div class="detail-title">Технические характеристики</div>
			<table class="detail-tech-table">
				<tr>
					<td>Размер рабочего стола</td>
					<td class="cnc-size">1300х2900х200</td>
				</tr>
				<tr>
					<td>Шпиндель</td>
					<td class="cnc-power">220V / 3 кВт</td>
				</tr>
				<tr>
					<td>Конструкция</td>
					<td class="cnc-constr">Стальные трубы квадратного сечения</td>
				</tr>
				<tr>
					<td>Опоры</td>
					<td class="cnc-support">6 тонких опор</td>
				</tr>
				<tr>
					<td>Передача</td>
					<td class="cnc-gear">Рейка и шестерня</td>
				</tr>
				<tr>
					<td>Система управления</td>
					<td class="cnc-contr">NCStudio</td>
				</tr>
				<tr>
					<td>Cтол</td>
					<td class="cnc-table">Без вакуумного насоса</td>
				</tr>
				<tr>
					<td>Привод</td>
					<td class="cnc-drive">Сервопривод</td>
				</tr>
				<tr>
					<td>Электрошкаф</td>
					<td class="cnc-cabinet">самостоятельный</td>
				</tr>
				<tr>
					<td>Размер в упаковке</td>
					<td class="cnc-sizebr">3200x2100x1800</td>
				</tr>
				<tr>
					<td>Вес</td>
					<td class="cnc-weight">1000кг</td>
				</tr>
			</table>
		</div>
		<div class="detail-video-block">
			<div class="detail-title">Видео по работе станка</div>
			
			<div id="model-video">
				<iframe width="391" height="238" src="https://www.youtube.com/embed/nrAfbpavlSk" frameborder="0" allowfullscreen></iframe>
			</div>
			
			<div class="detail-price-wrap w-clear">
				<div class="detail-price-title">Цена:</div>
				<div class="detail-price"><span class="">519 000</span> руб.</div>
				<a href="#order" class="detail-buy-button">Купить</a>
			</div>
			<a href="#footer" class="detail-price-more">Получить подробную информацию</a>
		</div>
	</div>
	<div class="detail-options">
		<div class="detail-options-title">Комплектация</div>
		<table class="detail-options-table" cellspacing="8">
			<tr>
				<td>Шасси</td>
				<td>Электрошкаф</td>
				<td>Соединительные кабели</td>
				<td>Набор фрез (6 шт)</td>
			</tr>
			<tr>
				<td>Шпиндель</td>
				<td>Паспорт станка</td>
				<td>Программное обеспечение</td>
				<td>Датчик положения высоты</td>
			</tr>
			<tr>
				<td>Набор цанг</td>
				<td>Прижимы (4 шт)</td>
				<td>Ящик с инструментами</td>
				
			</tr>
		</table>
		
		<div class="detail-options-notice">
			<div class="notice-text">
				При необходимости комплектации фрезерного станка дополнительными опциями, их список согласовывается при заказе и включается в комплектацию.
			</div>
			<a href="#order" class="notice-button">Список дополнительных опций</a>
		</div>
		
	</div>
</div>