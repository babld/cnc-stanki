<div class="options-block" id="order">
	<div class="options-container">
		<div class="options-container-title">Заказать фрезерно-гравировальный станок с ЧПУ</div>
		<div class="options-form-block">
			<form>
				<div class="option-left">
					<div class="option-image">
						<img src="/images/gallery/cx1325.png">
					</div>
					<div class="option-select select-wrap">
						<div class="controls">
							<div class="arrow"></div>
							<div class="label">Superstar CX1325</div>
						</div>
						<select>
							<option value="cx1325">Superstar CX1325</option>
							<option value="cxm1325">Superstar CXM1325</option>
							<option value="m25">Superstar M25</option>
							<option value="cxg6090">Superstar CXG6090</option>
						</select>
					</div>
					<table class="option-info">
						<tr>
							<td>Размер рабочего стола</td>
							<td class="cnc-size">1300x2900x200</td>
						</tr>
						<tr>
							<td>Шпиндель</td>
							<td class="cnc-power">220V / 3 кВт</td>
						</tr>
						<tr>
							<td>Стол</td>
							<td class="cnc-table">без вакуумного насоса</td>
						</tr>
					</table>
				</div>
				<div class="option-center">
					<div class="option-center-title">Нужны дополнительные опции?</div>
					<ul id="options-list">
						<li>
							<input type="checkbox" id="checkbox1"/>
							<label for="checkbox1">Аспирационная система <span>+ 14 900 руб.</span></label>
						</li>
						<li>
							<input type="checkbox" id="checkbox2"/>
							<label for="checkbox2">Система автоматической смазки <span>+ 15 200 руб.</span></label>
						</li>
						<li>
							<input type="checkbox" id="checkbox3"/>
							<label for="checkbox3">Смена NCstudio на пульт DSP <span>+ 20 800 руб.</span></label>
						</li>
						<li>
							<input type="checkbox" id="checkbox4"/>
							<label for="checkbox4">Вакуумный стол <span>+ 61 000 руб.</span></label>
						</li>
						<li>
							<input type="checkbox" id="checkbox5"/>
							<label for="checkbox5">Смена шпинделя на 3 кВт 380 V<span>+ 6 200 руб.</span></label>
						</li>
						<li>
							<input type="checkbox" id="checkbox6"/>
							<label for="checkbox6">Смена шпинделя на 4.5 кВт 380 V<span>+ 14 570 руб.</span></label>
						</li>
						<li>
							<input type="checkbox" id="checkbox7"/>
							<label for="checkbox7">Смена шпинделя на 5.5 кВт 380 V<span>+ 25 420 руб.</span></label>
						</li>
					</ul>
					<div class="options-center-price-block">
						<div class="options-center-price-text">Цена:</div>
						<div class="option-center-price"><span><script>document.write(cx1325Price)</script></span> руб.</div>
					</div>
				</div>
				<div class="option-right">
					<input name="model" type="hidden" value="" class="option-hidden-model"/>
					<div class="option-form-name">
						<input name="name" placeholder="Имя"/>
					</div>
					<div class="option-form-email">
						<input name="email" placeholder="E-mail"/>
					</div>
					<div class="option-form-phone">
						<input name="phone" placeholder="Телефон"/>
					</div>
					<div class="option-form-textarea">
						<textarea name="message" placeholder="Комментарий (Не обязательно)"></textarea>
					</div>
					<input type="submit" value="Заказать станок" class="option-form-submit"/>
				</div>
			</form>
		</div>
	</div>
</div>