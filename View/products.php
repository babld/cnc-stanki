<div class="products-block">
	<div class="products-container">
		<div class="products-container-title">
			Поверхности, обрабатываемые фрезерно-гравировальными станками с ЧПУ
		</div>
		<table class="products-table">
			<tr>
				<td><div class="products-table-title">Дерево</div></td>
				<td><div class="products-table-title">ДСП, МДФ</div></td>
				<td><div class="products-table-title">Искусственный камень</div></td>
				<td><div class="products-table-title">Алюкобонд</div></td>
				<td><div class="products-table-title">Текстолит</div></td>
				<td><div class="products-table-title">ПВХ</div></td>
				<td><div class="products-table-title">Оргстекло</div></td>
				<td><div class="products-table-title">Алюминий</div></td>
				<td><div class="products-table-title">Цветные металлы</div></td>
			</tr>
			<tr>
				<td><img src="/images/surfaces/wood.png"/></td>
				<td><img src="/images/surfaces/dsp.png"/></td>
				<td><img src="/images/surfaces/stone.png"/></td>
				<td><img src="/images/surfaces/aluc.png"/></td>
				<td><img src="/images/surfaces/txtlite.png"/></td>
				<td><img src="/images/surfaces/pvh.png"/></td>
				<td><img src="/images/surfaces/orglass.png"/></td>
				<td><img src="/images/surfaces/alum.png"/></td>
				<td><img src="/images/surfaces/nmetal.png"/></td>
			</tr>
		</table>
	</div>
	
	<div class="products-gallery-container">
		<div class="products-container-title">
			Фотографии изделий,<br/> изготавливаемых с помощью фрезерных станков с ЧПУ
		</div>
		<ul id="gallery">
			<li class="loaded">
				<a href="/images/products/1-full.jpg">
					<img src="/images/products/1.jpg">
				</a>
			</li>
			<li class="loaded">
				<a href="/images/products/2-full.jpg">
					<img src="/images/products/2.jpg">
				</a>
			</li>
			<li class="loaded">
				<a href="/images/products/3-full.jpg">
					<img src="/images/products/3.jpg">
				</a>
			</li>
			<li class="loaded">
				<a href="/images/products/4-full.jpg">
					<img src="/images/products/4.jpg">
				</a>
			</li>
			<li class="loaded no-mr">
				<a href="/images/products/5-full.jpg">
					<img src="/images/products/5.jpg">
				</a>
			</li>
			<li class="loaded">
				<a href="/images/products/6-full.jpg">
					<img src="/images/products/6.jpg">
				</a>
			</li>
			<li class="loaded">
				<a href="/images/products/7-full.jpg">
					<img src="/images/products/7.jpg">
				</a>
			</li>
			<li class="loaded">
				<a href="/images/products/8-full.jpg">
					<img src="/images/products/8.jpg">
				</a>
			</li>
			<li class="loaded">
				<a href="/images/products/9-full.jpg">
					<img src="/images/products/9.jpg">
				</a>
			</li>
			<li class="loaded no-mr">
				<a href="/images/products/10-full.jpg">
					<img src="/images/products/10.jpg">
				</a>
			</li>
			<li class="loaded">
				<a href="/images/products/11-full.jpg">
					<img src="/images/products/11.jpg">
				</a>
			</li>
			<li class="loaded">
				<a href="/images/products/12-full.jpg">
					<img src="/images/products/12.jpg">
				</a>
			</li>
			<li class="loaded">
				<a href="/images/products/13-full.jpg">
					<img src="/images/products/13.jpg">
				</a>
			</li>
			<li class="loaded">
				<a href="/images/products/14-full.jpg">
					<img src="/images/products/14.jpg">
				</a>
			</li>
			<li class="loaded no-mr">
				<a href="/images/products/15-full.jpg">
					<img src="/images/products/15.jpg">
				</a>
			</li>
			<li class="loaded">
				<a href="/images/products/16-full.jpg">
					<img src="/images/products/16.jpg">
				</a>
			</li>
			<li class="loaded">
				<a href="/images/products/17-full.jpg">
					<img src="/images/products/17.jpg">
				</a>
			</li>
			<li class="loaded">
				<a href="/images/products/18-full.jpg">
					<img src="/images/products/18.jpg">
				</a>
			</li>
			<li class="loaded">
				<a href="/images/products/19-full.jpg">
					<img src="/images/products/19.jpg">
				</a>
			</li>
			<li class="loaded no-mr">
				<a href="/images/products/20-full.jpg">
					<img src="/images/products/20.jpg">
				</a>
			</li>
		</ul>
		<div class="clear"></div>
	</div>
	<script>
		$('#gallery').photobox('a');
		// or with a fancier selector and some settings, and a callback:
		$('#gallery').photobox('a:first', { thumbs:true, time:0 }, imageLoaded);
		function imageLoaded(){
			console.log('image has been loaded...');
		}
	</script>
</div>