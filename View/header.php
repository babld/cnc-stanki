<?php
	$mail = 'info@cnc-stanki.ru';
	$kodermail = '';
	for($j = 0; $j < strlen($mail); $j++) {
		$kodermail .= '&#' . ord(substr($mail, $j, 1)) . ';';
	} ?>
<!DOCTYPE html>
<html>
	<head>
		<title>Купить фрезерный станок с ЧПУ SuperStar</title>
		<meta charset="utf-8"/>
		<meta name="keywords" content=""/>
		<meta name="description" content=""/>
		<link rel="stylesheet" href="/css/style.css"/>
		<link rel="stylesheet" href="/css/photobox.css"/>
		<link rel="stylesheet" href="/css/overlay.css"/>
		<link rel="stylesheet" href="/css/jquery.fancybox.css"/>
		<script src="/js/jquery-1.11.2.min.js"></script>
		<script src="/js/jquery.photobox.js"></script>
		<script src="/js/jquery.validate.min.js"></script>
		<script src="/js/jquery-ui.min.js"></script>
		<script src="/js/jquery.fancybox.pack.js"></script>
		<script src="/js/script.js"></script>
		<link rel="icon" href="/favicon.ico" type="image/x-icon">
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		
		<script src="/js/jquery.staFeedback.js"></script>
		<script src="http://api-maps.yandex.ru/2.1-dev/?lang=ru-RU&load=package.full" type="text/javascript"></script>
		<meta name='yandex-verification' content='47061948bbdd4ead' />
		<script type="text/javascript">
			totalPrice = 0;
			offices = [];
			offices["msk"] = [];
			offices["msk"] = ["Москва", 55.793353332847, 37.591329961971 , "Москва, ул. Сущевский Вал, 5, стр. 3."];
			offices["nsk"] = [];
			offices["nsk"] = ["Новосибирск", 55.04329723164, 82.949294433604 , "630112, Новосибирск, пр. Дзержинского, 1/1, 5 этаж, оф. 71"];
			offices["spb"] = [];
			offices["spb"] = ["Санкт-Петербург", 59.851078540186,30.476580279762 , "Санкт-Петербург. просп. Обуховской Обороны, 271"];
			offices["omsk"] = [];
			offices["omsk"] = ["Омск", 54.972178999995,73.402495347221 , "Омск. ул. Маяковского, 81"];
			offices["ekb"] = [];
			offices["ekb"] = ["Екатеринбург", 56.86482360733,60.593191728836 , "Екатеринбург. ул. Завокзальная 5а, офис 306"];
			
			function ymapsLoader(city) {
				ymaps.ready(function () {
					
					var ymapsContainer = "YMapsID";
					
					$(".city-switcher").parent().parent().removeClass("active");
					$(".city-switcher[data-city=" + city + "]").parent().parent().addClass("active");
					
					$("#" + ymapsContainer).html("");
					$("#" + ymapsContainer).html("<div class=\"ymaps-block-address\">" + offices[city][3] + "</div>");
					
					var myMap = new ymaps.Map(ymapsContainer, {
						center: [offices[city][1], offices[city][2]],
						zoom: 16,
						controls: []
					});

					myMap.controls.add('zoomControl');
					
					var myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
						balloonContentBody: [
							'<address>',
							'<strong>Офис "СибирьТрансАзия" в городе ' + offices[city][0] + '</strong>',
							'<br/>',
							'Тел: <?=MAINPHONE?>',
							'<br/>',
							'Адрес: ' + offices[city][3],
							'<br/>',
							'Подробнее: <a onclick="yaCounter28054470.reachGoal(\'link_to_sta\');return true;" target="_blank" href="http://sibtransasia.ru/">http://sibtransasia.ru/</a>',
							'</address>'
						].join('')
					}, {
						preset: 'islands#blueDotIcon'
					});
					myMap.geoObjects.add(myPlacemark);
					myPlacemark = null;
					myMap = null;
				});
			}
			
			cncModels = [];
			var cx1325Price = 8190;
			var cxm1325Price = 9140;
			var m25Price = 13035;
			var cxg6090Price = 3880;
			
			cx1325Price		= calcRubPrice(cx1325Price);
			cxm1325Price	= calcRubPrice(cxm1325Price);
			m25Price		= calcRubPrice(m25Price);
			cxg6090Price	= calcRubPrice(cxg6090Price);
			
			cncModels["cx1325"] = [
				"1300х2500х200",
				"220V / 3 кВт",
				"Стальные трубы квадратного сечения",
				"6 тонких опор",
				"Рейка и шестерня",
				"NCStudio",
				"Без вакуумного насоса",
				"Сервопривод",
				"самостоятельный",
				"3200x2100x1800",
				"1000кг"
			];
			cncModels["cx1325"]["index"] = 0;
			cncModels["cx1325"]["price"] = cx1325Price;
			cncModels["cx1325"]["video"] = '<iframe width="391" height="238" src="https://www.youtube.com/embed/1QysVFCweZ8" frameborder="0" allowfullscreen></iframe>';
			cncModels["cx1325"]["options"] = '\
						<li>\
							<input value="Аспирационная система" name="options[]" type="checkbox" id="checkbox1"/>\
							<label for="checkbox1">Аспирационная система <span class="option-price-wrap">+ <span class="option-price">' + calcRubPrice(240) + '</span> руб.</span></label>\
						</li>\
						<li>\
							<input value="Система автоматической смазки" name="options[]" type="checkbox" id="checkbox2"/>\
							<label for="checkbox2">Система автоматической смазки <span class="option-price-wrap">+ <span class="option-price">' + calcRubPrice(245) + '</span> руб.</span></label>\
						</li>\
						<li>\
							<input value="Смена NCstudio на пульт DSP" type="checkbox" name="options[]" id="checkbox3"/>\
							<label for="checkbox3">Смена NCstudio на пульт DSP <span class="option-price-wrap">+ <span class="option-price">' + calcRubPrice(335) + '</span> руб.</span></label>\
						</li>\
						<li>\
							<input value="Вакуумный стол" name="options[]" type="checkbox" id="checkbox4"/>\
							<label for="checkbox4">Вакуумный стол <span class="option-price-wrap">+ <span class="option-price">' + calcRubPrice(985) + '</span> руб.</span></label>\
						</li>\
						<li class="option-radio">\
							<input value="Смена шпинделя на 3 кВт 380 V" type="checkbox" name="options[]" id="checkbox5"/>\
							<label for="checkbox5">Смена шпинделя на 3 кВт 380 V<span class="option-price-wrap">+ <span class="option-price">' + calcRubPrice(100) + '</span> руб.</span></label>\
						</li>\
						<li class="option-radio">\
							<input value="Смена шпинделя на 4.5 кВт 380 V" name="options[]" type="checkbox" id="checkbox6"/>\
							<label for="checkbox6">Смена шпинделя на 4.5 кВт 380 V<span class="option-price-wrap">+ <span class="option-price">' + calcRubPrice(135) + '</span> руб.</span></label>\
						</li>\
						<li class="option-radio">\
							<input value="Смена шпинделя на 5.5 кВт 380 V" name="options[]" type="checkbox" id="checkbox7"/>\
							<label for="checkbox7">Смена шпинделя на 5.5 кВт 380 V<span class="option-price-wrap">+ <span class="option-price">' + calcRubPrice(175) + '</span> руб.</span></label>\
						</li>';
			
			cncModels["cx1325"]["img"] = [
				"/images/gallery/cx1325.png",
				"/images/gallery/cx1325-2.jpg",
				"/images/gallery/cx1325-3.jpg",
				"/images/gallery/thumb-cx1325.png",
				"/images/gallery/thumb-cx1325-2.jpg",
				"/images/gallery/thumb-cx1325-3.jpg",
				"/images/models/superstar-cx1325-full.jpg",
				"/images/models/superstar-cx1325-full2.jpg",
				"/images/models/superstar-cx1325-full3.jpg"
			]
			
			cncModels["cxm1325"] = [
				"1300х2500х200",
				"220V / 3 кВт",
				"Стальные трубы квадратного сечения",
				"4 толстых ноги",
				"Рейка и шестерня",
				"NCStudio",
				"Без вакуумного насоса",
				"Сервопривод",
				"самостоятельный",
				"3200x2100x1800",
				"1200кг"
			];
			cncModels["cxm1325"]["index"] = 1;
			cncModels["cxm1325"]["price"] = cxm1325Price;
			cncModels["cxm1325"]["video"] = '<iframe width="391" height="238" src="https://www.youtube.com/embed/fpvH8-WX0-w" frameborder="0" allowfullscreen></iframe>';
			cncModels["cxm1325"]["options"] = '\
						<li>\
							<input value="Аспирационная система" name="options[]" type="checkbox" id="checkbox1"/>\
							<label for="checkbox1">Аспирационная система <span class="option-price-wrap">+ <span class="option-price">' + calcRubPrice(240) + '</span> руб.</span></label>\
						</li>\
						<li>\
							<input value="Система автоматической смазки" name="options[]" type="checkbox" id="checkbox2"/>\
							<label for="checkbox2">Система автоматической смазки <span class="option-price-wrap">+ <span class="option-price">' + calcRubPrice(245) + '</span> руб.</span></label>\
						</li>\
						<li>\
							<input value="Смена NCstudio на пульт DSP" type="checkbox" name="options[]" id="checkbox3"/>\
							<label for="checkbox3">Смена NCstudio на пульт DSP <span class="option-price-wrap">+ <span class="option-price">' + calcRubPrice(335) + '</span> руб.</span></label>\
						</li>\
						<li>\
							<input value="Вакуумный стол" name="options[]" type="checkbox" id="checkbox4"/>\
							<label for="checkbox4">Вакуумный стол <span class="option-price-wrap">+ <span class="option-price">' + calcRubPrice(985) + '</span> руб.</span></label>\
						</li>\
						<li class="option-radio">\
							<input value="Смена шпинделя на 3 кВт 380 V" type="checkbox" name="options[]" id="checkbox5"/>\
							<label for="checkbox5">Смена шпинделя на 3 кВт 380 V<span class="option-price-wrap">+ <span class="option-price">' + calcRubPrice(100) + '</span> руб.</span></label>\
						</li>\
						<li class="option-radio">\
							<input value="Смена шпинделя на 4.5 кВт 380 V" name="options[]" type="checkbox" id="checkbox6"/>\
							<label for="checkbox6">Смена шпинделя на 4.5 кВт 380 V<span class="option-price-wrap">+ <span class="option-price">' + calcRubPrice(135) + '</span> руб.</span></label>\
						</li>\
						<li class="option-radio">\
							<input value="Смена шпинделя на 5.5 кВт 380 V" name="options[]" type="checkbox" id="checkbox7"/>\
							<label for="checkbox7">Смена шпинделя на 5.5 кВт 380 V<span class="option-price-wrap">+ <span class="option-price">' + calcRubPrice(175) + '</span> руб.</span></label>\
						</li>';
			
			cncModels["cxm1325"]["img"] = [
				"/images/gallery/cxm1325.jpg",
				"/images/gallery/cxm1325-2.jpg",
				"/images/gallery/cxm1325-3.jpg",
				"/images/gallery/thumb-cxm1325.jpg",
				"/images/gallery/thumb-cxm1325-2.jpg",
				"/images/gallery/thumb-cxm1325-3.jpg",
				"/images/models/superstar-cxm1325-full.jpg",
				"/images/models/superstar-cxm1325-full2.jpg",
				"/images/models/superstar-cxm1325-full3.jpg"
			]
			
			cncModels["m25"] = [
				"1300х2500х200",
				"380V / 4.5 кВт",
				"Стальные трубы квадратного сечения",
				"6 толстых опор",
				"Рейка и шестерня",
				"DSP",
				"С вакуумным столом",
				"Сервопривод",
				"самостоятельный",
				"3200x2100x1800",
				"1500кг"
			];
			cncModels["m25"]["index"] = 2;
			cncModels["m25"]["price"] = m25Price;
			cncModels["m25"]["video"] = '<iframe width="391" height="238" src="https://www.youtube.com/embed/L_6TAGiPGk8" frameborder="0" allowfullscreen></iframe>';
			cncModels["m25"]["options"] = '\
						<li>\
							<input value="Аспирационная система" name="options[]" type="checkbox" id="checkbox1"/>\
							<label for="checkbox1">Аспирационная система <span class="option-price-wrap">+ <span class="option-price">' + calcRubPrice(240) + '</span> руб.</span></label>\
						</li>\
						<li>\
							<input value="5,5 кВт шпиндель (возд. охлаждением)" name="options[]" type="checkbox" id="checkbox2"/>\
							<label for="checkbox2">5,5 кВт шпиндель (возд. охлаждением)<span class="option-price-wrap">+ <span class="option-price">' + calcRubPrice(175) + '</span> руб.</span></label>\
						</li>';
			
			cncModels["m25"]["img"] = [
				"/images/gallery/m25.jpg",
				"/images/gallery/m25-2.jpg",
				"/images/gallery/m25-3.jpg",
				"/images/gallery/thumb-m25.jpg",
				"/images/gallery/thumb-m25-2.jpg",
				"/images/gallery/thumb-m25-3.jpg",
				"/images/models/superstar-m25-full.jpg",
				"/images/models/superstar-m25-full2.jpg",
				"/images/models/superstar-m25-full3.jpg"
			];
			
			cncModels["cxg6090"] = [
				"600х900х150",
				"220V / 1.5 кВт",
				"Стальные трубы квадратного сечения",
				"стальные ноги",
				"Рейка и шестерня",
				"NCStudio",
				"Без вакуумного насоса",
				"Сервопривод",
				"встроенный",
				"1250x1000x1600",
				"250кг"
			];
			
			cncModels["cxg6090"]["index"] = 3;
			cncModels["cxg6090"]["price"] = cxg6090Price;
			cncModels["cxg6090"]["video"] = '<iframe width="391" height="238" src="https://www.youtube.com/embed/XWJ5krslVN8" frameborder="0" allowfullscreen></iframe>';
			cncModels["cxg6090"]["options"] = '\
						<li>\
							<input value="Аспирационная система" name="options[]" type="checkbox" id="checkbox1"/>\
							<label for="checkbox1">Аспирационная система <span class="option-price-wrap">+ <span class="option-price">' + calcRubPrice(240) +'</span> руб.</span></label>\
						</li>\
						<li>\
							<input value="Сменить Ncstudio на пульт DSP" name="options[]" type="checkbox" id="checkbox2"/>\
							<label for="checkbox2">Сменить Ncstudio на пульт DSP <span class="option-price-wrap">+ <span class="option-price">' + calcRubPrice(335) + '</span> руб.</span></label>\
						</li>';
			
			cncModels["cxg6090"]["img"] = [
				"/images/gallery/cxg6090.jpg",
				"/images/gallery/cxg6090-2.jpg",
				"/images/gallery/cxg6090-3.jpg",
				"/images/gallery/thumb-cxg6090.jpg",
				"/images/gallery/thumb-cxg6090-2.jpg",
				"/images/gallery/thumb-cxg6090-3.jpg",
				"/images/models/superstar-cxg6090-full.jpg",
				"/images/models/superstar-cxg6090-full2.jpg",
				"/images/models/superstar-cxg6090-full3.jpg"
			];
			
			//Fix для опредления браузера 
			jQuery.browser = {};
			jQuery.browser.mozilla = /mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit/.test(navigator.userAgent.toLowerCase());
			jQuery.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase());
			jQuery.browser.opera = /opera/.test(navigator.userAgent.toLowerCase());
			jQuery.browser.msie = /msie/.test(navigator.userAgent.toLowerCase());

			$(document).ready(function() {
				$(".detail-thumb a").click(function(){
					
					var model = $(this).parent().parent().parent().find(".detail-main-image img").attr("data-model");
					var index = $(this).find("img").attr("data-index");
					
					$(this).parent().parent().parent().find(".detail-main-image img").attr("src", $(this).attr("href"));
					$(this).parent().parent().parent().find(".detail-main-image a").attr("href", cncModels[model]["img"][parseInt(index) + 5]);
					return false;
				});
				
				$('a[href^="#"]').click(function () {
					var model = $(this).attr("data-model");
					elementClick = $(this).attr("href");
					destination = $(elementClick).offset().top;
					if(jQuery.browser.mozilla){
						$('html').animate( { scrollTop: destination }, 1100 );
					} else {
						$('body').animate( { scrollTop: destination }, 1100 );
					}
					
					if (model != undefined) {
						changeTech(model);
						changeImg(model);
						changeVideo(model);
						changeOptions(model);
						changeSelect(model);
						$($(".select-wrap.option-select select option").get(cncModels[model]["index"])).attr("selected", "selected").change();
					}

					return false;
				});
				
				$('.select-wrap').each(function(){
					var $this = $(this);
					$(this).find('select')
						.change(function(){
							$('.controls .label').text($this.find('select option:selected').text());
							
							var model = $this.find("option:selected").val();
							$($(".select-wrap.option-select select option").get(cncModels[model]["index"])).attr("selected", "selected");
							changeTech(model);
							changeImg(model);
							changeVideo(model);
							changeOptions(model);
						})
						.change();
				});
				
				$(".other-order, .other-choose-order").click(function(){
					$(".footet-title").text($(this).attr("data-info"));
				});
			});
			
			function changeTech(model) {
				$(".cnc-size").text(cncModels[model][0]);
				$(".cnc-power").text(cncModels[model][1]);
				$(".cnc-constr").text(cncModels[model][2]);
				$(".cnc-support").text(cncModels[model][3]);
				$(".cnc-gear").text(cncModels[model][4]);
				$(".cnc-contr").text(cncModels[model][5]);
				$(".cnc-table").text(cncModels[model][6]);
				$(".cnc-drive").text(cncModels[model][7]);
				$(".cnc-cabinet").text(cncModels[model][8]);
				$(".cnc-sizebr").text(cncModels[model][9]);
				$(".cnc-weight").text(cncModels[model][10]);
				$(".detail-price span").text(cncModels[model]["price"]);
				$(".option-center-price span").text(cncModels[model]["price"]);
				$(".option-hidden-model").val(model);
			}
			
			function changeImg(model) {
				$(".detail-main-image img").attr("src", cncModels[model]["img"][0]);
				$(".detail-main-image img").attr("data-model", model);
				$(".detail-main-image a").attr("href", cncModels[model]["img"][6]);
				
				$(".detail-thumb.first img").attr("src", cncModels[model]["img"][3]);
				$(".detail-thumb.second img").attr("src", cncModels[model]["img"][4]);
				$(".detail-thumb.third img").attr("src", cncModels[model]["img"][5]);
				
				$(".detail-thumb.first a").attr("href", cncModels[model]["img"][0]);
				$(".detail-thumb.second a").attr("href", cncModels[model]["img"][1]);
				$(".detail-thumb.third a").attr("href", cncModels[model]["img"][2]);
				
				$(".option-image img").attr("src", cncModels[model]["img"][0]);
				
				
			}
			
			function changeVideo(model) {
				$("#model-video iframe").remove();
				$("#model-video").append(cncModels[model]["video"]);
			}
			
			function changeOptions(model) {
				
				$("#options-list li").remove();
				$("#options-list").append(cncModels[model]["options"]);
				
				$("#options-list input[type=checkbox]").click(function(){
					
					//var optionPrice = $(this).parent().find(".option-price").text();
					
					var optionPrice = 0;
					var model = $(".option-select select option:selected").val();
					var cncPrice = 0;
					
					if ($(this).parent().hasClass("option-radio")) {
						if($(this).is(":checked") == true){
							$(this).parent().parent().find(".option-radio input[type=checkbox]").prop("checked", false);
							$(this).prop("checked", true);
						}
					}
					
					$("#options-list input[type=checkbox]:checked + label span.option-price").each(function(){
						var curOptionPrice = $(this).text();
						curOptionPrice = curOptionPrice.replace(/\s+/g, '');
						curOptionPrice = parseInt(curOptionPrice)
						optionPrice += curOptionPrice;
					});
					
					cncPrice = cncModels[model]["price"];
					cncPrice = cncPrice.replace(/\s+/g, '');
					cncPrice = parseInt(cncPrice);
					
					totalPrice = cncPrice + optionPrice;
					$(".option-center-price span").text(totalPrice.toString().replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1 "));
				});
			}
			
			function changeSelect(model) {
				//$('.controls .label').text($(".select-wrap select option:selected").text());
				var selectText = $(".select-wrap select option").get(cncModels[model]["index"]).text;
				$('.controls .label').text(selectText);
				
			}
			
			function calcRubPrice(usdPrice) {
				return (Math.round(usdPrice * <?=$usd?> * 0.01) * 100).toFixed(0).toString().replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1 ");
			}
		</script>

	</head>
	
	<body>
		<div class="header">
			<div class="header-box w-clear">
				<div class="header-logo">
					<a href="http://sibtransasia.ru" target="_blank">	
						<img src="/images/logo.png"/>
					</a>
				</div>
				<div class="main-title">
					Фрезерные станки с ЧПУ
				</div>
				<div class="header-contacts">
					<div class="w-clear">
						<div class="free-phone">
							<a href="tel:8-800-775-67-58">8-800-775-67-58</a>
						</div>
						<div class="free-phone-text">
							<a href="tel:8-800-775-67-58">звонок бесплатный</a>
						</div>
					</div>
					<div class="w-clear">
						<div class="city-phone">
							<a href="tel:8-383-207-88-60">8-383-207-88-60</a>
						</div>
						<div class="city-phone-text">
							<a href="tel:8-383-207-88-60">Новосибирск</a>
						</div>
					</div>
					<div class="w-clear">
						<div class="header-email-text">
							Эл. почта:
						</div>
						<div class="header-email">
							<a href="mailto:<?=$kodermail?>"><?=$kodermail?></a>
						</div>
					</div>
					<div class="">
						<div class="header-feedback">
							<form>
								<input class="feedback-phone" type="text" name="phone" placeholder="Ваш телефон"/>
								<input class="feedback-submit" type="submit" value="Перезвоните мне"/>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

	
	
