<div class="models" id="prices">
	
	<div class="models-container">
		
		<div class="cnc-model">
			<div class="model-image">
				<a href="/images/models/superstar-cx1325-full.jpg" class="fancybox"><img src="/images/models/superstar-cx1325.png"/></a>
			</div>
			<div class="model-tech">
				<div class="model-title">
					Универсальный фрезерно-гравировальный станок с ЧПУ
				</div>
				<div class="model-name">
					Superstar CX1325
				</div>
				<table class="tech-table">
					<tr>
						<td>Размер рабочего стола</td>
						<td><script>document.write(cncModels["cx1325"][0])</script></td>
					</tr>
					<tr>
						<td>Шпиндель</td>
						<td>220V / 3Квт</td>
					</tr>
					<tr>
						<td>Стол</td>
						<td>Без вакуумного стола</td>
					</tr>
				</table>
				<div class="model-price w-clear">
					<div class="price-title">Цена:</div>
					<div class="price"><script>document.write(cx1325Price)</script> руб.</div>
					<a href="#order" data-model="cx1325" class="model-order">Заказать</a>
				</div>
				<div class="model-more w-clear">
					<a href="#characteristics" data-model="cx1325" class="model-charac-but">Показать все характеристики</a>
					<a href="#sravnenie" class="model-match-but">Сравнить</a>
				</div>
			</div>
			
		</div>
		
		<div class="cnc-model no-mr">
			<div class="model-image">
				<a href="/images/models/superstar-cxm1325-full.jpg" class="fancybox"><img src="/images/models/superstar-cxm1325.png"/></a>
			</div>
			<div class="model-tech">
				<div class="model-title">
					Фрезерно-гравировальный станок с ЧПУ с усиленной конструкцией
				</div>
				<div class="model-name">
					Superstar CXМ1325
				</div>
				<table class="tech-table">
					<tr>
						<td>Размер рабочего стола</td>
						<td><script>document.write(cncModels["cxm1325"][0])</script></td>
					</tr>
					<tr>
						<td>Шпиндель</td>
						<td>220V / 3Квт</td>
					</tr>
					<tr>
						<td>Стол</td>
						<td>Без вакуумного стола</td>
					</tr>
				</table>
				<div class="model-price w-clear">
					<div class="price-title">Цена:</div>
					<div class="price"><script>document.write(cxm1325Price)</script> руб.</div>
					<a href="#order" data-model="cxm1325" class="model-order">Заказать</a>
				</div>
				<div class="model-more w-clear">
					<a href="#characteristics" data-model="cxm1325" class="model-charac-but">Показать все характеристики</a>
					<a href="#sravnenie" class="model-match-but">Сравнить</a>
				</div>
			</div>
		</div>
		
		<div class="cnc-model">
			<div class="model-image">
				<a href="/images/models/superstar-m25-full.jpg" class="fancybox"><img src="/images/models/superstar-m25.png"/></a>
			</div>
			<div class="model-tech">
				<div class="model-title">
					Промышленный фрезерно-гравировальный станок с ЧПУ
				</div>
				<div class="model-name">
					Superstar М25
				</div>
				<table class="tech-table">
					<tr>
						<td>Размер рабочего стола</td>
						<td><script>document.write(cncModels["m25"][0])</script></td>
					</tr>
					<tr>
						<td>Шпиндель</td>
						<td>220V / 4.5 кВт</td>
					</tr>
					<tr>
						<td>Стол</td>
						<td>Вакуумный стол</td>
					</tr>
				</table>
				<div class="model-price w-clear">
					<div class="price-title">Цена:</div>
					<div class="price"><script>document.write(m25Price)</script> руб.</div>
					<a href="#order" data-model="m25" class="model-order">Заказать</a>
				</div>
				<div class="model-more w-clear">
					<a href="#characteristics" data-model="m25" class="model-charac-but">Показать все характеристики</a>
					<a href="#sravnenie" class="model-match-but">Сравнить</a>
				</div>
			</div>
		</div>
		
		<div class="cnc-model no-mr">
			<div class="model-image">
				<a href="/images/models/superstar-cxg6090-full.jpg" class="fancybox"><img src="/images/models/superstar-cxg6090.png"/></a>
			</div>
			<div class="model-tech">
				<div class="model-title">
					Настольный фрезерно-гравировальный станок с ЧПУ
				</div>
				<div class="model-name">
					Superstar CXG 6090
				</div>
				<table class="tech-table">
					<tr>
						<td>Размер рабочего стола</td>
						<td><script>document.write(cncModels["cxg6090"][0])</script></td>
					</tr>
					<tr>
						<td>Шпиндель</td>
						<td>220V / 1.5 кВт</td>
					</tr>
					<tr>
						<td>Стол</td>
						<td>Без вакуумного стола</td>
					</tr>
				</table>
				<div class="model-price w-clear">
					<div class="price-title">Цена:</div>
					<div class="price"><script>document.write(cxg6090Price)</script> руб.</div>
					<a href="#order" data-model="cxg6090" class="model-order">Заказать</a>
				</div>
				<div class="model-more w-clear">
					<a href="#characteristics" data-model="cxg6090" class="model-charac-but">Показать все характеристики</a>
					<a href="#sravnenie" class="model-match-but">Сравнить</a>
				</div>
			</div>
		</div>
		
	</div>
	
</div>
<?php


	


?>