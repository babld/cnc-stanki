<div class="our-advantages-block">
	<div class="our-advantages">
		<div class="our-advantages-title">Почему стоит заказать именно у нас?</div>
		<ul class="our-advantages-list w-clear">
			<li>
				<div class="advantage-icon best-price"></div>
				<div class="advantage-title">Отличные цены</div>
				<div class="advantage-text">Вы можете в этом убедиться самостоятельно, рассчитав стоимость фрезерного станка с ЧПУ в специальной форме.</div>
			</li>
			<li>
				<div class="advantage-icon dropship"></div>
				<div class="advantage-title">Работа без посредников</div>
				<div class="advantage-text">Мы закупаем фрезерные станки у производителя, самостоятельно доставляем в Россию и проводим процедуру таможенного оформления. Зачем платить посредникам?</div>
			</li>
			<li>
				<div class="advantage-icon quality"></div>
				<div class="advantage-title">Гарантированное качество</div>
				<div class="advantage-text">Фабрика Jinan Superstar Machinery Manufactore - надежный производитель отлично зарекомендовавший себя в России. На все станки имеется вся необходимая документация</div>
			</li>
			<li>
				<div class="advantage-icon fix-price"></div>
				<div class="advantage-title">Гарантированные цены</div>
				<div class="advantage-text">Знакома ситуация, когда Вам дают одну цену, а по факту приходится платить больше? Где угодно, только не у нас!</div>
			</li>
			<li>
				<div class="advantage-icon"></div>
				<div class="advantage-title">Быстрая доставка</div>
				<div class="advantage-text">Срок поставки с учетом производства, транспортировки по Китаю и доставки до Вашего города составит всего 30-45 дней</div>
			</li>
			<li>
				<div class="advantage-icon logist"></div>
				<div class="advantage-title">Удобная логистика</div>
				<div class="advantage-text">Работаем только автомобильным транспортом, своя инфраструктура на маршруте, оперативно отправляем станки с ЧПУ в любой город России!</div>
			</li>
			<li>
				<div class="advantage-icon doc-pack"></div>
				<div class="advantage-title">Полный пакет документов</div>
				<div class="advantage-text">Заказывая фрезерные станки с ЧПУ у нас, Вы получаете полный комплект документов для постановки товара на учет.</div>
			</li>
			<li>
				<div class="advantage-icon duration"></div>
				<div class="advantage-title">Доставляем фрезерные станки с ЧПУ 7 лет</div>
				<div class="advantage-text">Компания СибирьТрансАзия - профессиональный участник ВЭД. Мы доставляем станки с ЧПУ 7 лет и хорошо знаем как надо работать с Китаем</div>
			</li>
		</ul>
	</div>
</div>
