<div class="our_work">
	<div class="our_work-inner">
		<div class="header-w">Как мы работаем</div>
		<ul class="w-clear our_work-icons">
			<li>
				<i></i>
				<div class="our_work-icon lead"></div>
				<div class="our_work-item">Отправка заявки</div>
			</li>
			<li>
				<i></i>
				<div class="our_work-icon info"></div>
				<div class="our_work-item">Уточнение информации</div>
			</li>
			<li>
				<i></i>
				<div class="our_work-icon contract"></div>
				<div class="our_work-item">Заключение договора</div>
			</li>
			<li>
				<i></i>
				<div class="our_work-icon prepay"></div>
				<div class="our_work-item">Частичная предоплата</div>
			</li>
			<li>
				<i></i>
				<div class="our_work-icon deliver"></div>
				<div class="our_work-item">Доставка в Россию</div>
			</li>
			<li>
				<i></i>
				<div class="our_work-icon postpay"></div>
				<div class="our_work-item">Остаток оплаты</div>
			</li>
			<li class="no-mr">
				<div class="our_work-icon get"></div>
				<div class="our_work-item">Получение станков с ЧПУ в РФ</div>
			</li>
		</ul>
	</div>
</div>